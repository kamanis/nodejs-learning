function Stack() {
	this.items = [];
}

Stack.prototype.toString = function() {
	return (this.items.length > 0) ? JSON.stringify(this.items) : 'No items';
}

Stack.prototype.isEmpty = function() {
	return (this.items.length === 0) ? true : false;
}

Stack.prototype.top = function() {
	return (!this.isEmpty()) ? this.items[this.items.length-1] : null;
}

Stack.prototype.itemCount = function() {
	return (!this.isEmpty()) ? this.items.length : 0;
}

Stack.prototype.pushItem = function(item) {
	this.items.push(item);
}

Stack.prototype.pop = function() {
	return (!this.isEmpty()) ? this.items.pop() : null;
}

module.exports = Stack;