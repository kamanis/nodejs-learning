var GraphNode = require('./graph-node.js');

/*
	This is Graph Class
 */
function Graph() {
	this.nodes = [];
}

Graph.prototype.getAllNodes = function() {
	return this.nodes;
}

Graph.prototype.findNode = function(value) {
	for (var loop=0; loop < this.nodes.length; loop++)
	{
		var node = this.nodes[loop];
		if ((node instanceof GraphNode) && (node.value == value))
		{
			return node;
		}

	}

	return false;
};

Graph.prototype.forEach = function(callback) {
	for(var loop=0; loop < this.nodes.length; loop++)
	{
		callback(this.nodes[loop]);
	}
};

Graph.prototype.printAllNode = function() {
	this.forEach(function (node) {
		node.printDetail();
	})	
};

Graph.prototype.addNode = function(value) {
	if(!this.findNode(value))
	{
		var newNode = new GraphNode(value);

		this.nodes.push(newNode);

		return newNode;
	}

	return null;
};

Graph.prototype.addEdge = function(toNode, fromNode, weight) {
	if ((toNode != undefined) && (fromNode != undefined))
	{
		toNode.addEdge(fromNode.value, weight);
	}
};

Graph.prototype.resetGraphTraverse = function() {
	for (var loop=0; loop < this.nodes.length; loop++)
	{
		var node = this.nodes[loop];
		if (node instanceof GraphNode)
		{
			node.resetExplore(); 
		}
	}
};

module.exports =  Graph;