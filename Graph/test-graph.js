var Graph = require('./graph.js');
var GraphNode = require('./graph-node.js');
var DFSGraphTraverse = require('./dfs-graph.js');
var BFSGraphTraverse = require('./bfs-graph.js');

var directedGraph = new Graph();

var nodeA = directedGraph.addNode('A');
var nodeB = directedGraph.addNode('B');
var nodeC = directedGraph.addNode('C');
var nodeD = directedGraph.addNode('D');
var nodeE = directedGraph.addNode('E');
var nodeF = directedGraph.addNode('F');

directedGraph.addEdge(nodeA, nodeB, 5);
directedGraph.addEdge(nodeB, nodeC, 4);
directedGraph.addEdge(nodeC, nodeD, 8);
directedGraph.addEdge(nodeD, nodeC, 8);
directedGraph.addEdge(nodeD, nodeE, 6);
directedGraph.addEdge(nodeA, nodeD, 5);
directedGraph.addEdge(nodeC, nodeE, 2);
directedGraph.addEdge(nodeE, nodeB, 3);
directedGraph.addEdge(nodeA, nodeE, 7);
directedGraph.addEdge(nodeE, nodeF, 9);

directedGraph.printAllNode();

var dfsGraphTraversal = new DFSGraphTraverse(directedGraph);
dfsGraphTraversal.traverse(nodeB);

directedGraph.resetGraphTraverse();
directedGraph.printAllNode();

var bfsGraphTraversal = new BFSGraphTraverse(directedGraph);
bfsGraphTraversal.traverse(nodeB);