require('../../graph.js');
var GraphEdge = require('../../graph-edge.js');
var GraphNode = require('../../graph-node.js');
var Stack = require('../../../Stack/stack.js');

function BuildOrder(graph)
{
	this.stack = new Stack();
	this.sequence = [];
	this.graph = graph;

	this.findBuildOrder = function()
	{
		var sequence = [];
		var allBuildNodes = this.graph.getAllNodes();

		for(var nodeLoop=0; nodeLoop<allBuildNodes.length; nodeLoop++)
		{
			var buildNode = allBuildNodes[nodeLoop];
			if(!buildNode.isExplore)
			{
				buildNode.temporaryTraverse = true;
				this.stack.pushItem(buildNode);
				console.log('Push Item --> ' + buildNode.value);
				while(!this.stack.isEmpty())
				{
					var topNode = this.stack.top();
					console.log('Top Item --> ' + topNode.value);
					var allEdges = topNode.edges;

					var isAllChildVisited = true; 
					if(allEdges.length > 0)
					{
						for(var edgeLoop=0; edgeLoop<allEdges.length; edgeLoop++)
						{
							var dependentBuilds = allEdges[edgeLoop];
							var dependentBuildNode = this.graph.findNode(dependentBuilds.connectedNodeValue);
							//dependentBuildNode.printDetail();
							if(!dependentBuildNode.isExplore)
							{
								if(!dependentBuildNode.temporaryTraverse)
								{
									isAllChildVisited = false;
									console.log('Push Dependent Item --> ' + dependentBuildNode.value);
									dependentBuildNode.temporaryTraverse = true;
									this.stack.pushItem(dependentBuildNode);
								}
								else
								{
									console.log('Find Cyclic Between Item --> ' + topNode.value + ', and  Item --> ' + dependentBuildNode.value);
									return -1; // find cyclic
								}
							}
						}
					}
					
					if(isAllChildVisited)
					{
						var popNode = this.stack.pop();
						console.log('Pop Item --> ' + popNode.value);

						popNode.printDetail();
						this.sequence.push(popNode);
						popNode.isExplore = true;
					}
				}
			}
		}


		console.log('-------------------------');
		console.log('Build Order implemention Solution');
		this.sequence.forEach(function (node) {
			node.printDetail();
		});
		console.log('-------------------------');
	}
}

module.exports =  BuildOrder;