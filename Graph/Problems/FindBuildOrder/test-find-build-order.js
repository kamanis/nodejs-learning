var Graph = require('../../graph.js');
var GraphNode = require('./find-build-order-node.js');
var FindBuildOrder = require('./find-build-order.js');

var directedGraph = new Graph();

var nodeA = directedGraph.addNode('0');
var nodeB = directedGraph.addNode('1');
var nodeC = directedGraph.addNode('2');
var nodeD = directedGraph.addNode('3');
var nodeE = directedGraph.addNode('4');

//directedGraph.addEdge(nodeB, nodeC, 1);
//directedGraph.addEdge(nodeC, nodeB, 4);
//directedGraph.addEdge(nodeD, nodeB, 8);
//directedGraph.addEdge(nodeD, nodeC, 8);
//directedGraph.addEdge(nodeE, nodeD, 6);

directedGraph.addEdge(nodeA, nodeB, 1);
directedGraph.addEdge(nodeB, nodeC, 1);
directedGraph.addEdge(nodeB, nodeD, 1);
directedGraph.addEdge(nodeD, nodeE, 1);


directedGraph.printAllNode();

var findBuildOrder = new FindBuildOrder(directedGraph);
findBuildOrder.findBuildOrder();

