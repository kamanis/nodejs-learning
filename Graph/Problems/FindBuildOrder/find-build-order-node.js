var GraphNode = require('../../graph-node.js');

function BuildOrderGraphNode()
{
	this.temporaryTraverse = false;
}

// Use prototype to associate GraphNode as the base object for BuildOrderGraphNode
BuildOrderGraphNode.prototype = new GraphNode();

module.exports =  BuildOrderGraphNode;

