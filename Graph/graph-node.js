var GraphEdge = require('./graph-edge.js');

/*
	This is Graph Node Class
 */
function GraphNode(value) {
	this.value = value;
	this.edges = [];
	this.isExplore = false;
}

GraphNode.prototype.printDetail = function() {
	console.log('GraphNode : Value -> ' + this.value + ' and Edges to -> ' + ((this.edges.length > 0) ? this.edges.join(', ') : 'No Connected Node'));
};

GraphNode.prototype.addEdge = function(connectedNodeValue, weight) {
	var newEdge = new GraphEdge(weight);
	newEdge.connectedNodeValue = connectedNodeValue;

	this.edges.push(newEdge);
};

GraphNode.prototype.resetExplore = function() {
	this.isExplore = false
};

module.exports =  GraphNode;