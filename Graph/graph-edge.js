/*
	This is Graph Edge Class
 */
function GraphEdge(weight) {
	this.weight = weight;
	this.connectedNodeValue = '';
}

GraphEdge.prototype.toString = function() {
	return (this.connectedNodeValue.length > 0) ? this.connectedNodeValue+this.weight : 'Unknown';
};

module.exports =  GraphEdge;