require('./graph.js');
var GraphEdge = require('./graph-edge.js');
var GraphNode = require('./graph-node.js');
var Stack = require('../Stack/stack.js');

function DFSGraphTraverse(graph) {
	this.stack = new Stack();
	this.sequence = [];
	this.graph = graph;


	this.traverse = function(graphNode)
	{
		if(graphNode)
		{
			this.stack.pushItem(graphNode);
			graphNode.isExplore = true;

			while(!this.stack.isEmpty())
			{
				var node = this.stack.pop();
				if (node instanceof GraphNode)
				{
					this.sequence.push(node);
					for (var loop=0; loop < node.edges.length; loop++)
					{	
						var edge = node.edges[loop];
						if (edge instanceof GraphEdge)
						{
							var edgeNode = this.graph.findNode(edge.connectedNodeValue);
							if(!edgeNode.isExplore)
							{
								edgeNode.isExplore = true;
								this.stack.pushItem(edgeNode);
							}
						}
					}
				}
			}
		}

		console.log('-------------------------');
		console.log('DFS implemention Solution');
		this.sequence.forEach(function (node) {
			node.printDetail();
		});
		console.log('-------------------------');
	}

}

module.exports =  DFSGraphTraverse;