require('./graph.js');
var GraphEdge = require('./graph-edge.js');
var GraphNode = require('./graph-node.js');
var Queue = require('../Queue/queue.js');

function BFSGraphTraverse(graph) {
	this.queue = new Queue();
	this.sequence = [];
	this.graph = graph;


	this.traverse = function(graphNode)
	{
		if(graphNode)
		{
			this.queue.enqueue(graphNode);
			graphNode.isExplore = true;

			while(!this.queue.isEmpty())
			{
				var node = this.queue.dequeue();
				if (node instanceof GraphNode)
				{
					this.sequence.push(node);
					for (var loop=0; loop < node.edges.length; loop++)
					{	
						var edge = node.edges[loop];
						if (edge instanceof GraphEdge)
						{
							var edgeNode = this.graph.findNode(edge.connectedNodeValue);
							if(!edgeNode.isExplore)
							{
								edgeNode.isExplore = true;
								this.queue.enqueue(edgeNode);
							}
						}
					}
				}
			}
		}

		console.log('-------------------------');
		console.log('BFS implemention Solution');
		this.sequence.forEach(function (node) {
			node.printDetail();
		});
		console.log('-------------------------');
	}

}

module.exports =  BFSGraphTraverse;