// Given 2 strings, write a function to determine whether they are anagrams.

function findAnagrams(str1, str2)
{
  if (str1.length !== str2.length)  return false;
  
  var characterCount = {};

  for (var loop=0; loop<str1.length; loop++)
  {
    var value = characterCount[str1[loop]];
    characterCount[str1[loop]] = ((value) && (value > 0)) ? value++ : 1;
  }

  for(var loop1=0; loop1<str2.length; loop1++)
  {
    var value1 = characterCount[str2[loop1]];
    if((value1) && (value1 > 0))
    {
      value1--;
    }
    else
      return false;
  }

  return true;
}

function perfromTest()
{
  //return (findAnagrams('abc', 'cba') === true);
  return ((findAnagrams('', '') === true) &&
          (findAnagrams('abc', 'cba') === true) &&
          (findAnagrams('', 'cba') === false) &&
          (findAnagrams('abc', '') === false) &&
          (findAnagrams('abcd', 'cba') === false) && 
          (findAnagrams('abcd', 'cbae') === false) &&
          (findAnagrams('abcdf', 'fcbad') === true) &&
          (findAnagrams('abcdfY', 'fcybad') === false) &&
          (findAnagrams('abcdfZ', 'fcZbad') === true) &&
          (findAnagrams('ab2cdfZ', 'fcZba2d') === true) &&
          (findAnagrams('ab2cd1fZ', 'f3cZba2d') === false)
          );
}

if(perfromTest())
{
  console.log("All test passed successfully");
}
else
{
  console.log("Test failed. Check your solution or test");
}
