var Tree = require('./tree.js');
var TreeNode = require('./tree-node.js');
var Queue = require('../Queue/queue.js');

function BinaryTree() {
}

BinaryTree.prototype = new Tree();

BinaryTree.prototype.addNode = function(node) {
	if(node instanceof TreeNode )
	{
		console.log('Add node of value ' + node.value + ' in binary tree.');
		if(this.root === null)
		{
			this.root = node 
		}
		else
		{
			var queue = new Queue();
			queue.enqueue(this.root);

			while (!queue.isEmpty())
			{
				var tempPtr = queue.dequeue();
				if(tempPtr)
				{
					if(!tempPtr.left)
					{
						tempPtr.left = node;
						break;
					}
					else if(!tempPtr.right)
					{
						tempPtr.right = node;
						break;
					}
					else
					{
						queue.enqueue(tempPtr.left);
						queue.enqueue(tempPtr.right);
					}
				}
			}
		}
	}	
}

module.exports =  BinaryTree;