/*
	This is Tree Node Class
 */
function TreeNode(value) {
	this.value = value;
	this.left = null;
	this.right = null;
}

TreeNode.prototype.printDetail = function() {
	console.log('TreeNode : Value -> ' + this.value + ' and Left node to -> ' + ((this.left) ? this.left.value : 'No Left Node') + ', and right node to -> ' + ((this.right) ? this.right.value : 'No Right Node'));
};

module.exports =  TreeNode;