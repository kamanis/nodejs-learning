var Tree = require('./tree.js');
var TreeNode = require('./tree-node.js');

function BinarySearchTree() {

}

BinarySearchTree.prototype = new Tree();

BinarySearchTree.prototype.addNode = function(node) {
	if(node instanceof TreeNode )
	{
		console.log('Add node of value ' + node.value + ' in binary tree.');
		if(this.root === null)
		{
			this.root = node 
		}
		else
		{
			insertNodeInBST(this.root, node);
		}
	}	
}

function insertNodeInBST(root, newNode)
{
	if(root.value > newNode.value)
	{
		if(!root.left) 
		{
			root.left = newNode;
			return;
		}
		else
			insertNodeInBST(root.left, newNode);
	}
	else
	{
		if(!root.right) 
		{
			root.right = newNode;
			return;
		}
		else
			insertNodeInBST(root.right, newNode);
	}
}

module.exports =  BinarySearchTree;