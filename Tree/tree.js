var TreeNode = require('./tree-node.js');

//var TreeNode = require('./graph-node.js');

function Tree() {
	this.root = null;
}

Tree.prototype.traverseTree = function(traversalType) {
	if(traversalType === 0) // 0 means in-order traversal
	{
		console.log('-------------------------');
		console.log('In-order Traversal Solution');
		inOrderTraversal(this.root);
		console.log('-------------------------');
	}
	else if(traversalType === 1) // 1 means pre-order traversal
	{
		console.log('-------------------------');
		console.log('Pre-order Traversal Solution');
		preOrderTraversal(this.root);
		console.log('-------------------------');
	}	
	else if(traversalType === 2) // 2 means post-order traversal
	{
		console.log('-------------------------');
		console.log('Post-order Traversal Solution');
		postOrderTraversal(this.root);
		console.log('-------------------------');

	}		
}

Tree.prototype.isBST = function() {

	return (this.root) ? isBST(this.root) : true;
}

function inOrderTraversal(root) {
	if(root === null) return;

	inOrderTraversal(root.left);
	root.printDetail();
	inOrderTraversal(root.right);
}

function preOrderTraversal(root) {
	if(root === null) return;

	root.printDetail();
	preOrderTraversal(root.left);
	preOrderTraversal(root.right);
}

function postOrderTraversal(root) {
	if(root === null) return;

	postOrderTraversal(root.left);
	postOrderTraversal(root.right);
	root.printDetail();
}

function isBST(root) {
	var value = true;
	if(root.left)
	{
		if(root.value < root.left.value)
		{
			return false;
		}

		value = isBST(root.left);
	}
	if(root.right)
	{
		if(root.value >= root.right.value)
		{
			return false;
		}

		value = isBST(root.right);
	}

	return value;
}

module.exports =  Tree;