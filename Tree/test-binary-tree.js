var TreeNode = require('./tree-node.js');
var BinaryTree = require('./binary-tree.js');

var binaryTree = new BinaryTree();

binaryTree.addNode(new TreeNode(4));
binaryTree.addNode(new TreeNode(3));
binaryTree.addNode(new TreeNode(7));
binaryTree.addNode(new TreeNode(12));
binaryTree.addNode(new TreeNode(3));
binaryTree.addNode(new TreeNode(6));
binaryTree.addNode(new TreeNode(2));

binaryTree.traverseTree(1); // print pre order traversal
console.log('Is binary Tree a BST ' + binaryTree.isBST());

var BinarySearchTree = require('./binary-search-tree.js');

var binarySearchTree = new BinarySearchTree();

binarySearchTree.addNode(new TreeNode(4));
binarySearchTree.addNode(new TreeNode(3));
binarySearchTree.addNode(new TreeNode(7));
binarySearchTree.addNode(new TreeNode(12));
binarySearchTree.addNode(new TreeNode(3));
binarySearchTree.addNode(new TreeNode(6));
binarySearchTree.addNode(new TreeNode(2));

binarySearchTree.traverseTree(0); // print in order traversal
console.log('Is Binary Search tree a BST ' + binarySearchTree.isBST());