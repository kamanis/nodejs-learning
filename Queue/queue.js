function Queue() {
	this.items = [];
}

Queue.prototype.toString = function() {
	return (this.items.length > 0) ? JSON.stringify(this.items) : 'No items';
}

Queue.prototype.isEmpty = function() {
	return (this.items.length === 0) ? true : false;
}

Queue.prototype.front = function() {
	return (!this.isEmpty()) ? this.items[0] : null;
}

Queue.prototype.itemCount = function() {
	return (!this.isEmpty()) ? this.items.length : 0;
}

Queue.prototype.enqueue = function(item) {
	this.items.push(item);
}

Queue.prototype.dequeue = function() {
	return (!this.isEmpty()) ? this.items.shift() : null;
}

module.exports = Queue;