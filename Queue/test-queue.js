var queue = new (require('./queueUsingTwoStack.js'))();

function test_SingleItem()
{
    queue.reset();
    queue.enqueue(4);

    return (queue.dequeue() === 4) 
            ? true : false;
}

function test_TwoItems()
{
    queue.reset();
    queue.enqueue(4);
    queue.enqueue(16);

    return ((queue.dequeue() === 4) && 
            (queue.dequeue() === 16))
            ? true : false;
}

function test_ThreeItems()
{
    queue.reset();
    queue.enqueue(4);
    queue.enqueue(16);
    queue.enqueue(50);

    return ((queue.dequeue() === 4) && 
            (queue.dequeue() === 16) && 
            (queue.dequeue() === 50))
            ? true : false;
}

function test_FailureCases()
{
    queue.reset();
    queue.enqueue(4);
    queue.enqueue(16);
    queue.enqueue(50);

    return ((queue.dequeue() === 4) && 
            (queue.dequeue() === 50) && 
            (queue.dequeue() === 50))
            ? false : true;
}

function isAllTestPassed()
{
    return (test_SingleItem() &&
            test_TwoItems() &&
            test_ThreeItems() &&
            test_FailureCases()) 
            ? true : false;
}

if(isAllTestPassed())
{
    console.log('All test passed successfully');
}
else
{
    console.log('Test Failed');
}