var Stack = require('../Stack/stack.js');

function Queue() {
    this.defaultStack = new Stack();
    this.tempStack = new Stack();
}

Queue.prototype.toString = function() {
	return (this.defaultStack.isEmpty()) ? 'No items' : JSON.stringify(this.defaultStack.toString());
}

Queue.prototype.isEmpty = function() {
	return (this.defaultStack.isEmpty()) ? true : false;
}

Queue.prototype.front = function() {
	return (!this.defaultStack.isEmpty()) ? this.defaultStack.top() : null;
}

Queue.prototype.itemCount = function() {
	return this.defaultStack.itemCount() + this.tempStack.isEmpty();
}

Queue.prototype.enqueue = function(item) {
    this.defaultStack.pushItem(item);
}

Queue.prototype.dequeue = function() {
    if (!this.tempStack.isEmpty())
    {
        return this.tempStack.pop();
    }
    else
    {
        while(!this.defaultStack.isEmpty())
        {
            var popItem = this.defaultStack.pop();

            if(popItem)
            {
                this.tempStack.pushItem(popItem);
            }
        }

        return this.tempStack.pop();
    }
}

Queue.prototype.reset = function() {
    while(!this.defaultStack.isEmpty())
    {
        this.defaultStack.pop();
    }

    while(!this.tempStack.isEmpty())
    {
        this.tempStack.pop();
    }
}

module.exports = Queue;