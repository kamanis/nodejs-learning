function JavascriptClass()
{
	var privateVar = 'hey!  i am private variable. You cannot use me, by using object of linkedlist into another classes.';
	
	var privateTestFunction = function() {
		console.log('');
		console.log('---------- privateTestFunction Start ----------');
		console.log('---------- privateTestFunction End ----------');
		console.log('');
	}


	this.publicVar = 'hey!  i am public variable. You can use me, by using object of linkedlist into another classes.';

	this.publicTestFunction = function publicTestFunction()
	{
		console.log('');
		console.log('---------- publicTestFunction Start ----------');
		// Public functions can use private variables.
		console.log('Print Private Variable -> ' + privateVar);
		console.log('Print Public Variable -> ' + this.publicVar);
		console.log('---------- publicTestFunction End ----------');
		console.log('');

		// Public functions can use private function.
		privateTestFunction();
	}

	
}

JavascriptClass.prototype.accessPrivateVariableInPrototypeMethod = function() {
	console.log('');
	console.log('---------- accessPrivateVariableInPrototypeMethod Start ----------');
	console.log('Print Private Variable -> ' + this.privateVar);
	console.log('---------- accessPrivateVariableInPrototypeMethod End ----------');
	console.log('');
};

JavascriptClass.prototype.accessPublicVariablenPrototypeMethod = function() {
	console.log('');
	console.log('---------- accessPublicVariablenPrototypeMethod Start ----------');
	console.log('Print Public Variable -> ' + this.publicVar);
	console.log('---------- accessPublicVariablenPrototypeMethod End ----------');
	console.log('');
};

module.exports =  JavascriptClass;