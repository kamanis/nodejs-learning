var javascriptClass = new (require('./javascript-class.js'))();

console.log('Print Private Variable --> ' + javascriptClass.privateVar);
console.log('Print Public Variable --> ' + javascriptClass.publicVar);

javascriptClass.accessPrivateVariableInPrototypeMethod();
javascriptClass.accessPublicVariablenPrototypeMethod();

javascriptClass.publicTestFunction();

// Cannot use private functions using object.
//javascriptClass.privateTestFucntion();
//

var Employee = require('./Inheritance/employee.js');
var PermenantEmployee = require('./Inheritance/permenant-employee.js');

// Create employee object
var employee = new Employee('Mark');

console.log('Employee name --> ' + employee.getName());
if(employee instanceof Employee)
  console.log('employee is instanceof Employee');
else
  console.log('employee is not instanceof Employee'); 

// in the console instanceof method is always returning false.
console.log('employee is instanceof Employee --> ' + typeof employee);

// Create permenant employee object
var permenantEmployee = new PermenantEmployee(10000);
console.log('Permenant Employee name --> ' + permenantEmployee.getName());

if(permenantEmployee instanceof Employee)
  console.log('permenantEmployee is instanceof Employee');
else
  console.log('permenantEmployee is not instanceof Employee');

  if(permenantEmployee instanceof PermenantEmployee)
  console.log('permenantEmployee is instanceof PermenantEmployee');
else
  console.log('permenantEmployee is not instanceof PermenantEmployee');