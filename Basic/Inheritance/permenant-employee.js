var Employee = require('./employee.js');

// PermenantEmployee will be the driver object
var PermenantEmployee = function(annualSalary)
{
  this.annualSalary = annualSalary;
  this.name = 'johnny';
}

// Use prototype to associate Employee as the base object for PermenantEmployee
PermenantEmployee.prototype = new Employee();

module.exports =  PermenantEmployee;