
// Employee will be the base object
var Employee = function(name)
{
  this.name = name;
}

// getName() is added to base object (Employee)
Employee.prototype.getName = function(first_argument) {
  return this.name;
};


module.exports =  Employee;