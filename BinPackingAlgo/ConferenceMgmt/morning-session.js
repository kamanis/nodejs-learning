var Session = require('./session.js');

function MorningSession()
{
  this.name = 'Morning';
  this.talks = [];
  this.startTime = 9;
  this.endTime = 12;
}

MorningSession.prototype = new Session();

module.exports =  MorningSession;