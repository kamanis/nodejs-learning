/*
  This class sort data using quick sort algorithm.
 */
function QuickSort(data, compareKey) {
  this.data = data;
  this.compareKey = compareKey;
}

QuickSort.prototype.sortData = function(left, right) {
  var len = this.data.length, 
  pivot,
  partitionIndex;

  if(left < right) {
    pivot = right;
    partitionIndex = partition(this.data, pivot, left, right, this.compareKey);
    
    //sort left and right
    this.sortData(left, partitionIndex - 1);
    this.sortData(partitionIndex + 1, right);
  }
};

function partition(arr, pivot, left, right, compareKey)
{
  var pivotValue = arr[pivot], partitionIndex = left;

   for(var i = left; i < right; i++){
    if(arr[i][compareKey] > pivotValue[compareKey]){
      swap(arr, i, partitionIndex);
      partitionIndex++;
    }
  }
  swap(arr, right, partitionIndex);
  return partitionIndex;
}

function swap(arr, i, j){
   var temp = arr[i];
   arr[i] = arr[j];
   arr[j] = temp;
}

module.exports =  QuickSort;