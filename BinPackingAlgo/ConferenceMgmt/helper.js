var fs = require('fs');

/*
  This helper class has utility functions.
 */
function Helper() {

}

Helper.prototype.logAllCommandLineArgs = function() {
  process.argv.forEach(function (val, index, array) {
    console.log(index + ': ' + val);
  });
};

Helper.prototype.isReadInputFromFile = function() {
  var myArgs = process.argv.slice(2);

  var isReadInputFromFile = false;
  for(var loop=0; loop<myArgs.length; loop++)
  {
      if(myArgs[loop] === '-breadinputfromfile')
      {
          isReadInputFromFile = true;
      }
  }

  return isReadInputFromFile;
};

/*
  Read data from file stream.
 */
Helper.prototype.readInputFile = function(fileName, successCB, failureCB) {
  var data = '';

  var readStream = fs.createReadStream(fileName, 'utf8');

  readStream.on('data', function(chunk) {  
    data += chunk;
  }).on('end', function() {
    (successCB) ? successCB(data) : null;
  }).on('error', function(error){ 
    (failureCB) ? failureCB(error) : null; 
  });
};

/*
  Read file data line by line.
 */
Helper.prototype.readLineByLine = function(data, readLineCB, readCompletedCB) {
  var newLineIndex = 0;
  var line;

  while(newLineIndex != -1)
  {
    newLineIndex = data.indexOf('\n');
    line = '';

    if(newLineIndex != -1)
    {
        line = data.substring(0,newLineIndex);
        data = data.substring(newLineIndex+1, data.length);
    }
    else if((newLineIndex == -1) && (data.length > 0))
    {
        line = data;
    }

    if(line.length > 0)
    {
        readLineCB(line);
    }
  }

  readCompletedCB();
};

Helper.prototype.openWriteStream = function(fileName, callback) {
  var wstream = fs.createWriteStream(fileName);
  callback(wstream)
};

Helper.prototype.formatAMPM = function(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = ((hours < 10) ? ('0'+hours) : hours) + ':' + minutes + ampm;
  return strTime;
};

module.exports =  Helper;