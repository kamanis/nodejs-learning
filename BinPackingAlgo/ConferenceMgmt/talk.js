/*
	Talk Class
 */
function Talk(title, durationInMins) {
	this.title = title || '';
	this.durationInMins = durationInMins || 0;
}

Talk.prototype.toString = function() {
	return (this.title.length > 0) ? (this.title + ' ' + this.durationInMins + ((this.durationInMins > 1) ? 'mins' : 'min')) : '';
};

Talk.prototype.addTalk = function(title, durationInMins) {
	this.title = title;
	this.durationInMins = durationInMins;
};

module.exports =  Talk;