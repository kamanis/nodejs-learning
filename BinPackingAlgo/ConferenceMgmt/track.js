var MorningSession = require('./morning-session.js');
var AfternoonSession = require('./afternoon-session.js');
var LunchSession = require('./lunch-session.js');

/*
	Track Class
 */

function Track(title, date) {
	this.date = date;
	this.title = title || '';

	this.sessions = [];
	this.createSession('Morning');
	this.createSession('Lunch');
	this.createSession('Afternoon');
}

Track.prototype.createSession = function(sessionName)
{
	switch(sessionName)
	{
		case 'Morning':
			var morningSession = new MorningSession();
			morningSession.initializeRemainingTime();
			this.sessions.push(morningSession);
		break;
		case 'Lunch':
			var lunchSession = new LunchSession();
			this.sessions.push(lunchSession);
		break;
		case 'Afternoon':
			var afternoonSession = new AfternoonSession(17);
			afternoonSession.initializeRemainingTime();
			this.sessions.push(afternoonSession);
		break;
	}
}

Track.prototype.print = function(wstream) {
	console.log('');
	console.log(this.title);
	if(wstream)
	{
		wstream.write(this.title + '\n\n');
	}
	for(var sessionLoop=0; sessionLoop < this.sessions.length; sessionLoop++)
	{
		var session = this.sessions[sessionLoop];
		session.print(wstream);
	}
};

module.exports =  Track;