var helper = new (require('./helper.js'))();

/*
	Session Class
 */

function Session(startTime, endTime) {
	this.talks = [];
	this.startTime = startTime || 0;
	this.endTime = endTime || 0;
	this.remainingTime = 0;
}

Session.prototype.initializeRemainingTime = function() {
	this.remainingTime = (this.endTime > this.startTime) ? ((this.endTime - this.startTime) * 60) : 0;
};

Session.prototype.forEach = function(callback) {
	for (var loop=0; loop<this.talks.length; loop++)
	{
		callback(loop, this.talks[loop]);
	}
};

Session.prototype.print = function(wstream) {
	var hours = this.startTime;
	var minutes = 0;
	var date = new Date();
	date.setHours(hours);
	date.setMinutes(minutes);
	if(this.name === 'Lunch')
	{
		var lunchSessionOutput = helper.formatAMPM(date) + ' ' + this.name;
		console.log(lunchSessionOutput);
		(wstream) ? wstream.write(lunchSessionOutput + '\n') : null;
	}
	else
	{
		this.forEach(function(index, talk){
			var talkOutput = helper.formatAMPM(date) + ' ' + talk.toString();
			console.log(talkOutput);
			(wstream) ? wstream.write(talkOutput + '\n') : null;
			date = new Date(date.getTime() + talk.durationInMins*60000)
		});

		if(this.name === 'Afternoon')
		{
			var eventEndOutput = helper.formatAMPM(date) + ' Networking Event';
			console.log(eventEndOutput);
			(wstream) ? wstream.write(eventEndOutput + '\n\n') : null;
		}
	}
};

Session.prototype.addTalk = function(talk) {
	this.remainingTime -= talk.durationInMins;
	this.talks.push(talk);
};

Session.prototype.removeLastTalk = function() {
	var talk = this.talks.pop();
	if(talk)
	{
		this.remainingTime += talk.durationInMins;
	
		return talk;
	}
	return null;
};

Session.prototype.lastTalk = function() {
	if(this.talks.length > 0)
		return this.talks[this.talks.length - 1];
	return null;
};

module.exports =  Session;