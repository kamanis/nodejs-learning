var Track = require('./track.js');
var Talk = require('./talk.js');
var QuickSort =  require('./quick-sort.js');
var helper = new (require('./helper.js'))();

/*
	Conference Class
 */
function Conference(name) {
	this.name = name || 'General Conference';
	this.tracks = [];
	
	this.registeredTalks = [];
	this.sessions = [];

	this.loadDefaultTrack();
}

Conference.prototype.forEachTrack = function(callback) {
	for (var loop=0; loop<this.tracks.length; loop++)
	{
		callback(this.tracks[loop]);
	}
};

Conference.prototype.printSchedule = function(wstream) {
	this.forEachTrack(function(track){
		track.print(wstream);
	});
};

Conference.prototype.loadDefaultTrack = function() {
	var todayDate = new Date();
	var day1 = new Track('Track 1', todayDate);
	this.tracks.push(day1);

	var tomorrowDate = new Date();
	tomorrowDate.setDate(todayDate.getDate() + 1);
	var day2 = new Track('Track 2', tomorrowDate);
	this.tracks.push(day2);
};

Conference.prototype.addTrack = function(date) {
	var newTrack = new Track(date);
	if(newTrack instanceof Track)
		this.tracks.push(track);
};

Conference.prototype.forEachTalk = function(callback) {
	for (var loop=0; loop<this.registeredTalks.length; loop++)
	{
		callback(loop, this.registeredTalks[loop]);
	}
};

Conference.prototype.printRegisteredTalk = function() {
	console.log('');
	console.log('Test input:');
	this.forEachTalk(function(index, talk){
		console.log(index+1 + ': ' + talk.toString());
	});
	console.log('');
};

Conference.prototype.registerTalk = function(title, durationInMins) {
	var newTalk = new Talk(title, durationInMins);
	if(newTalk instanceof Talk)
		this.registeredTalks.push(newTalk);
};

Conference.prototype.initiateScheduling = function()
{
	this.loadAllSessions();

	this.printRegisteredTalk();
	
	var remainingTalks = this.registeredTalks;
	var needToProcessLaterTalk = [];
	for(var sessionLoop=0; sessionLoop < this.sessions.length; sessionLoop++)
	{
		var session = this.sessions[sessionLoop];
		if(session.remainingTime > 0)
		{
			//console.log('session name --> ' + session.name + ', remainingTime --> ' + session.remainingTime);
			
			var remainingTalksQuickSort = new QuickSort(remainingTalks, 'durationInMins');
			remainingTalksQuickSort.sortData(0, remainingTalks.length-1);

			/*for(var talkLoop=0; talkLoop < remainingTalks.length; talkLoop++)
			{
				var talk = remainingTalks[talkLoop];
				console.log(talkLoop+1 + ': ' + talk.toString());
			}*/
		
			for(var talkLoop=0; talkLoop < remainingTalks.length; talkLoop++)
			{
				var talk = remainingTalks[talkLoop];
				if(session.remainingTime >= talk.durationInMins)
				{
					session.addTalk(talk);
				}
				else if(session.remainingTime > 0)
				{
					var lastTalk = session.lastTalk(); // get session last tak
					if(lastTalk)
					{
						if(lastTalk.durationInMins > talk.durationInMins) // if last talk duration is greater than current talk, then do shuffling.
						{
							session.removeLastTalk(); // remove last talk from session.
							needToProcessLaterTalk.push(lastTalk); // move last talk into need to process later talks
							session.addTalk(talk); // add current talk into session
						}
						else
						{
							needToProcessLaterTalk.push(talk);
						}
					}
				}
				else if(session.remainingTime == 0)
				{
					needToProcessLaterTalk.push(talk);
				}

			}

			remainingTalks = needToProcessLaterTalk;
			needToProcessLaterTalk = [];
		}
	}

	/*for(var talkLoop=0; talkLoop < this.registeredTalks.length; talkLoop++)
	{
		var talk = this.registeredTalks[talkLoop];
		for(var trackLoop=0; trackLoop < this.tracks.length; trackLoop++)
		{
			var track = this.tracks[trackLoop];
			if(track.morningSession.remainingTime >= talk.durationInMins)
			{
				track.morningSession.addTalk(talk);
				break;
			}
			else if(track.afternoonSession.remainingTime >= talk.durationInMins)
			{
				track.afternoonSession.addTalk(talk);
				break;
			}
		}
	}*/
}

Conference.prototype.loadAllSessions = function() {
	for(var trackLoop=0; trackLoop < this.tracks.length; trackLoop++)
	{
		this.sessions = this.sessions.concat(this.tracks[trackLoop].sessions);
	};
};

module.exports =  Conference;