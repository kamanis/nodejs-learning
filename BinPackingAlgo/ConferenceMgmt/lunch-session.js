var Session = require('./session.js');

function LunchSession()
{
  this.name = 'Lunch';
  this.startTime = 12;
  this.endTime = 13;
}

LunchSession.prototype = new Session();


module.exports =  LunchSession;