var Session = require('./session.js');

function AfternoonSession(endTime)
{
  this.name = 'Afternoon';
  this.talks = [];
  this.startTime = 13;
  this.endTime = endTime;
}

AfternoonSession.prototype = new Session();

module.exports =  AfternoonSession;