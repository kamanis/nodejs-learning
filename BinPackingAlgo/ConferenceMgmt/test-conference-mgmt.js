var Conference = require('./conference.js');
var helper = new (require('./helper.js'))();

if(helper.isReadInputFromFile()) // based on command line argument '-breadinputfromfile', app will decide to read input from file and write output to file. Otherwise use hardcoded input and log only output in console.
{
    var inputFileName = 'input.txt';
    var outputFileName = 'output.txt';

    helper.readInputFile(inputFileName, function(data) {
        var conference = new Conference();

        helper.readLineByLine(data, function(line) {
            var words = line.split(' ');
        
            if(words.length > 0)
            {
                var word = words[words.length - 1];
                var minutesIndex = word.indexOf('min');
                
                var talkDuration = 60;
                var talkTitle = line;

                if(minutesIndex != -1)
                {
                    talkDuration = parseInt(word.substring(0,minutesIndex)) || 0;
                    talkTitle = words.slice(0,words.length-1).join(' ') || '';
                }

                //console.log('talkTitle --> ' + talkTitle + ', talkDuration --> ' + talkDuration);
                conference.registerTalk(talkTitle, talkDuration);
            }
        }, function() {
            conference.initiateScheduling();

            helper.openWriteStream(outputFileName, function(wstream) {
                wstream.write('Test output:' + '\n');
                console.log('Test output:');
                conference.printSchedule(wstream);
                wstream.end();
            });
        });
    }, function(error) {
        console.log(inputFileName + ' does not exit. Please check is input file exist.');
    });
}
else
{
    var conference = new Conference();
    conference.registerTalk('Writing Fast Tests Against Enterprise Rails', 60);
    conference.registerTalk('Overdoing it in Python', 45);
    conference.registerTalk('Lua for the Masses', 30);
    conference.registerTalk('Ruby Errors from Mismatched Gem Versions', 45);
    conference.registerTalk('Common Ruby Errors', 45);
    conference.registerTalk('Rails for Python Developers lightning', 60);
    conference.registerTalk('Communicating Over Distance', 60);
    conference.registerTalk('Accounting-Driven Development', 45);
    conference.registerTalk('Woah', 30);
    conference.registerTalk('Sit Down and Write', 30);
    conference.registerTalk('Pair Programming vs Noise', 45);
    conference.registerTalk('Rails Magic', 60);
    conference.registerTalk('Ruby on Rails: Why We Should Move On', 60);
    conference.registerTalk('Clojure Ate Scala (on my project)', 45);
    conference.registerTalk('Programming in the Boondocks of Seattle', 30);
    conference.registerTalk('Ruby vs. Clojure for Back-End Development', 30);
    conference.registerTalk('Ruby on Rails Legacy App Maintenance', 60);
    conference.registerTalk('A World Without HackerNews', 30);
    conference.registerTalk('User Interface CSS in Rails Apps', 30);

    conference.initiateScheduling();

    conference.printSchedule();
}