var LinkedList = require('./linkedlist.js');

var intergerList = new LinkedList();

console.log('Added Nodes Into List');
intergerList.addNode(1);
intergerList.addNode(10);
intergerList.addNode(5);
intergerList.addNode(12);
intergerList.addNode(16);
intergerList.addNode(17);
intergerList.addNode(9);

intergerList.print();

console.log('Remove First Node From List');
intergerList.removeNode(1);
intergerList.print();

console.log('Remove Last Node From List');
intergerList.removeNode(9);
intergerList.print();

console.log('Remove Middle Node From List');
intergerList.removeNode(12);
intergerList.print();