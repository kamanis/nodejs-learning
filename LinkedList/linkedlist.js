function Node(value) {
	this.value = value;
	this.next = null;
}

function LinkedList()
{
	this.head = null;
	this.tail = null;
}

LinkedList.prototype.forEach = function(callback) {
	var temp = this.head;
	while(temp)
	{
		callback(temp.value);
		temp = temp.next;
	}
};

LinkedList.prototype.print = function() {
	var result = [];
	this.forEach(function (value) {
		result.push(value);
	})
	console.log('Values -> ' + result.join(', '));
};

LinkedList.prototype.addNode = function(value) {
	var temp = new Node(value)

	if(!this.head)
	{
		this.head = temp;
		this.tail = temp;
	}
	else
	{
		this.tail.next = temp;
		this.tail = temp;
	}
};


LinkedList.prototype.removeNode = function(value) {
	if(this.head)
	{
		if(this.head.value == value)
		{
			this.head = this.head.next;
			return;
		}
		var temp = this.head;
		do {
			if(temp.next.value == value)
			{
				temp.next = temp.next.next;
				return;
			}
			temp = temp.next;
		} while(temp != this.tail)
	}
}

module.exports =  LinkedList;